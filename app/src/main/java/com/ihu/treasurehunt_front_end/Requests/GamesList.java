package com.ihu.treasurehunt_front_end.Requests;

import com.ihu.treasurehunt_front_end.Model.AvailableGames;

import org.jetbrains.annotations.NotNull;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class GamesList {

    private final List<AvailableGames> availableGames = new ArrayList<>();
    private Boolean addPlayer;

    public void  addPlayerToTheGame(JsonPlaceHolderAPI jsonPlaceHolderAPI,String userName,String id)
    {
        Call<Boolean> call = jsonPlaceHolderAPI.addPlayer(userName,id);
        call.enqueue(new Callback<Boolean>() {
            @Override
            public void onResponse(Call<Boolean> call, Response<Boolean> response) {
                if (response.isSuccessful()) {
                   addPlayer = response.body();
                }else
                    addPlayer = false;
            }

            @Override
            public void onFailure(Call<Boolean> call, Throwable t) {
                System.out.println(t.getMessage());
            }
        });
    }

    public List<AvailableGames> getAvailableGames(){return availableGames;}
    public void getAvailableGames(JsonPlaceHolderAPI jsonPlaceHolderAPI){
        Call<List<AvailableGames>> call =jsonPlaceHolderAPI.getAvailableGames();

        call.enqueue(new Callback<List<AvailableGames>>() {
            @Override
            public void onResponse(@NotNull  Call<List<AvailableGames>> call,@NotNull Response<List<AvailableGames>> response) {
                if(!response.isSuccessful()){
                    System.out.println("Code"+response.code());
                    return;
                }
                List<AvailableGames> availableGamesList = response.body();
                for (AvailableGames availableGame:availableGamesList){
                    availableGames.add(new AvailableGames(availableGame.getGameLocation(),availableGame.getid()));
                }

            }

            @Override
            public void onFailure(@NotNull Call<List<AvailableGames>> call,@NotNull Throwable t) {
                System.out.println(t.getMessage());

            }
        });

    }
    public List<AvailableGames> getList(){
        return availableGames;
    }
    public Boolean getAddPlayer(){return addPlayer;}
}
