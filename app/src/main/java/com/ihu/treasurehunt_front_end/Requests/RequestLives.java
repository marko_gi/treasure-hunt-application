package com.ihu.treasurehunt_front_end.Requests;

import com.ihu.treasurehunt_front_end.Model.User;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RequestLives {

    private int Lives;
    public int getLives() {return Lives;}

    public void getLives(JsonPlaceHolderAPI jsonPlaceHolderAPI, String userName){
        Call<Integer> call = jsonPlaceHolderAPI.getUserLives(userName);

        call.enqueue(new Callback<Integer>() {
            @Override
            public void onResponse(Call<Integer> call, Response<Integer> response) {
                if(!response.isSuccessful()){
                    System.out.println(response.code());
                    return;
                }
                RequestLives.this.Lives = response.body();
            }

            @Override
            public void onFailure(Call<Integer> call, Throwable t) {
                System.out.println(t.getMessage());
            }
        });
    }

}
