package com.ihu.treasurehunt_front_end.Service;

import com.ihu.treasurehunt_front_end.Model.GameState;
import com.ihu.treasurehunt_front_end.Model.PlayerLocation;
import com.ihu.treasurehunt_front_end.Model.TreasureHuntGame;
import com.ihu.treasurehunt_front_end.Model.User;
import com.ihu.treasurehunt_front_end.Requests.JsonPlaceHolderAPI;
import org.jetbrains.annotations.NotNull;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TreasureHuntGameService
{

    private String message;
    private TreasureHuntGame treasureHuntGame;

    public void setWinner(JsonPlaceHolderAPI jsonPlaceHolderAPI,User loginUser,String id) {
        Call<Void> call = jsonPlaceHolderAPI.setWinner(loginUser,id);
        call.enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
            }
            @Override
            public void onFailure(Call<Void> call, Throwable t) {

            }
        });
    }

    public void CreateGame(JsonPlaceHolderAPI jsonPlaceHolderAPI, String id, String title, String gameLocation)
    {
        if (isValidData(id,title,gameLocation)) {
            Call<Boolean> call = jsonPlaceHolderAPI.createGame(id, title, gameLocation);

            call.enqueue(new Callback<Boolean>() {
                @Override
                public void onResponse(@NotNull Call<Boolean> call, @NotNull Response<Boolean> response) {
                   if (response.isSuccessful())
                       message = "Game Created successful";
                   else
                       message = "Check your inputs";
                }
                @Override
                public void onFailure(@NotNull Call<Boolean> call, @NotNull Throwable t) {
                }
            });
        }
    }
    public void getGame(JsonPlaceHolderAPI jsonPlaceHolderAPI, String id)
    {
        Call<TreasureHuntGame> call = jsonPlaceHolderAPI.getGame(id);

        call.enqueue(new Callback<TreasureHuntGame>() {
            @Override
            public void onResponse(Call<TreasureHuntGame> call, Response<TreasureHuntGame> response) {
                if (response.isSuccessful()) {
                    treasureHuntGame = response.body();
                }
                else {
                    JSONObject jOb = null;
                    try {
                        jOb = new JSONObject(response.errorBody().string());
                    } catch (JSONException | IOException e) {
                        e.printStackTrace();
                    }
                    try {
                        message = jOb.get("message").toString();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<TreasureHuntGame> call, Throwable t) { }
        });
    }
    public void addPlayerLocation(JsonPlaceHolderAPI jsonPlaceHolderAPI, PlayerLocation playerLocation, String gameId)
    {
        Call<Void> call = jsonPlaceHolderAPI.addPlayersLocation(playerLocation,gameId);
        call.enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
            }
            @Override
            public void onFailure(Call<Void> call, Throwable t) {
            }
        });
    }

    public String generateID()
    {
        String AlphaNumericString = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
                + "0123456789"
                + "abcdefghijklmnopqrstuvxyz";
        StringBuilder sb = new StringBuilder(5);

        for (int i = 0; i < 5; i++) {
            int index = (int)(AlphaNumericString.length() * Math.random());
            sb.append(AlphaNumericString.charAt(index));
        }

        return sb.toString();
    }

    public Boolean isValidData(String id , String title ,String gameLocation)
    {

            return !id.equals("") &&!title.equals("") && !gameLocation.equals("");

    }
    public String getMessage() {
        return this.message;
    }

    public TreasureHuntGame getTreasureHuntGame() {
        return treasureHuntGame;
    }

    public void setTreasureHuntGame(TreasureHuntGame treasureHuntGame) {
        this.treasureHuntGame = treasureHuntGame;
    }

}
