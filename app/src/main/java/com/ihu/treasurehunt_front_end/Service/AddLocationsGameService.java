package com.ihu.treasurehunt_front_end.Service;
import com.ihu.treasurehunt_front_end.Model.GameLocation;
import com.ihu.treasurehunt_front_end.Requests.JsonPlaceHolderAPI;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddLocationsGameService
{
    private String message;

    public void AddLocation(JsonPlaceHolderAPI jsonPlaceHolderAPI, GameLocation location, String id)
    {
        Call<Boolean> call = jsonPlaceHolderAPI.insertLocation(location,id);
        call.enqueue(new Callback<Boolean>() {
            @Override
            public void onResponse(Call<Boolean> call, Response<Boolean> response) {
                if (response.isSuccessful())
                    message = "Locations store Successfully";
                else
                    try {
                        JSONObject jOb = new JSONObject(response.errorBody().string());
                        message = jOb.get("message").toString();
                    } catch (JSONException | IOException e) {
                        e.printStackTrace();
                    }
            }
            @Override
            public void onFailure(Call<Boolean> call, Throwable t) {
                t.printStackTrace();
            }
        });
    }

    public String getMessage()
    {
        return this.message;
    }
}
