package com.ihu.treasurehunt_front_end.Model;


import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.ihu.treasurehunt_front_end.Activities.SignInActivity;
import com.ihu.treasurehunt_front_end.Service.TreasureHuntGameService;

import java.util.ArrayList;
import java.util.List;

import static com.google.maps.android.SphericalUtil.computeDistanceBetween;


public class GameCalculations {

    private TreasureHuntGameService treasureHuntGameService;
    private int gameScore;
    private User userLoggedIn;
    private Question currentQuestion;
    private int currentPositionOfLocation = 0;
    private User loginUser = SignInActivity.loginUser;
    private List<Marker> oldMarkers;


    public GameCalculations(TreasureHuntGameService treasureHuntGameService) {
        this.treasureHuntGameService = treasureHuntGameService;
        this.gameScore = 0;
        this.userLoggedIn = SignInActivity.loginUser;
        oldMarkers = new ArrayList<>();
    }

    public void incPositionOfLocation()
    {
        this.currentPositionOfLocation++;
    }

    public Question getCurrentQuestion() {
        return currentQuestion;
    }

    public void setCurrentQuestion(Question currentQuestion) {
        this.currentQuestion = currentQuestion;
    }

    public TreasureHuntGameService getTreasureHuntGameService() {
        return treasureHuntGameService;
    }
    public TreasureHuntGame getTreasureHuntGame()
    {
        return treasureHuntGameService.getTreasureHuntGame();
    }

    public LatLng LocationPosition(){
        return new LatLng(
                this.treasureHuntGameService.getTreasureHuntGame().getLocations().get(this.currentPositionOfLocation).getV()
                ,this.treasureHuntGameService.getTreasureHuntGame().getLocations().get(this.currentPositionOfLocation).getV1());
    }

    public MarkerOptions markerOptions(){
        return new MarkerOptions().position(this.LocationPosition()).
                title(treasureHuntGameService.getTreasureHuntGame()
                        .getLocations().get(this.currentPositionOfLocation).getTitle());
    }

    public MarkerOptions markerOptions(PlayerLocation playerLocation ,String title){
        return new MarkerOptions().position(new LatLng(playerLocation.getV(),playerLocation.getV1())).
                title(title).icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_ORANGE));
    }


    public Marker addLocationToMap(GoogleMap map){
        Marker marker = map.addMarker(this.markerOptions());
        marker.setVisible(false);
        return marker;
    }

    public void DistanceBetween(LatLng latLng, Marker marker){
        double distance;
        distance = computeDistanceBetween(latLng,marker.getPosition());
        marker.setVisible(distance <= 100);
    }

    public List<PlayerLocation> OthersPlayersLocation()
    {
        List<PlayerLocation> list = new ArrayList<>();
        for (int i=0;i<treasureHuntGameService.getTreasureHuntGame().getPlayerLocations().size();i++)
        {
            if (!treasureHuntGameService.getTreasureHuntGame().getPlayerLocations().get(i).getPlayerUserName().equals(loginUser.getName()))
                list.add(treasureHuntGameService.getTreasureHuntGame().getPlayerLocations().get(i));
        }
        return list;
    }

    public void othersPlayersMarker(GoogleMap googleMap)
    {
        removeOldMarkers();
        List<PlayerLocation> locations = this.OthersPlayersLocation();
        if (locations.size() > 0)
        {
            for (int i=0; i<locations.size();i++)
            {
                Marker marker =googleMap.addMarker( this.markerOptions(locations.get(i),locations.get(i).getPlayerUserName()));
                oldMarkers.add(marker);
            }
        }
    }
    public void setTreasureHuntGame(TreasureHuntGame treasureHuntGame)
    {
        this.treasureHuntGameService.setTreasureHuntGame(treasureHuntGame);
    }
    public void removeOldMarkers() {
        if (!oldMarkers.isEmpty())
        for (int i=0; i<oldMarkers.size();i++){
            Marker marker =oldMarkers.get(i);
            marker.remove();
        }
        oldMarkers.clear();
    }

    public int getCurrentPositionOfLocation() {
        return currentPositionOfLocation;
    }

    public void addPoints(int points)
    {
        this.gameScore += points;
    }

    public int getGameScore()
    {
        return this.gameScore;
    }
}
