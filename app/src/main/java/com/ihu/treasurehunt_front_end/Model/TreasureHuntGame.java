package com.ihu.treasurehunt_front_end.Model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class TreasureHuntGame {

    @SerializedName("id")
    private String id;

    @SerializedName("gameName")
    private  String title;

    @SerializedName("gameLocation")
    private String location;

    @SerializedName("userList")
    private List<User> players;

    @SerializedName("gameLocationsList")
    private List<GameLocation> locations;

    @SerializedName("userPositionList")
    private List<PlayerLocation> playerLocations;

    @SerializedName("state")
    private GameState gameState;

    @SerializedName("winner")
    private User winner;

    public TreasureHuntGame(String id, String title, String location, List<User> players, List<GameLocation> locations, List<PlayerLocation> playerLocations, GameState gameState, User winner) {
        this.id = id;
        this.title = title;
        this.location = location;
        this.players = players;
        this.locations = locations;
        this.playerLocations = playerLocations;
        this.gameState = gameState;
        this.winner = winner;
    }

    public String getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getLocation() {
        return location;
    }

    public List<GameLocation> getLocations() {
        return locations;
    }

    public List<User> getPlayers() {
        return players;
    }

    public List<PlayerLocation> getPlayerLocations() {
        return playerLocations;
    }

    public User getWinner() {
        return winner;
    }

    public GameState getGameState() {
        return gameState;
    }

}
