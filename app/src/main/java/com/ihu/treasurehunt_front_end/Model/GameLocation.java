package com.ihu.treasurehunt_front_end.Model;

import com.google.gson.annotations.SerializedName;

public class GameLocation {

    @SerializedName("title")
    private final String title;

    @SerializedName("v1")
    private final double v;

    @SerializedName("v2")
    private final double v1;

    public GameLocation(double v, double v1, String title) {
        this.title =title;
        this.v = v;
        this.v1 = v1;

    }

    public double getV() {
        return v;
    }

    public double getV1() {
        return v1;
    }

    public String getTitle() {
        return title;
    }
}
