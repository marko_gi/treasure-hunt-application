package com.ihu.treasurehunt_front_end.Model;

import com.google.gson.annotations.SerializedName;

public class AvailableGames {


    @SerializedName("gameLocation")
    private String gameLocation;


    @SerializedName("id")
    private String id;


    public AvailableGames(String gameLocation , String  id)
    {
        this.gameLocation= gameLocation;
        this.id = id;


    }
    public String getGameLocation(){
        return gameLocation;

    }
    public String getid(){
        return id;

    }
}
