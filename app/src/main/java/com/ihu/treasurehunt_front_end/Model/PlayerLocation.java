package com.ihu.treasurehunt_front_end.Model;

import com.google.gson.annotations.SerializedName;

public class PlayerLocation {

    @SerializedName("userName")
    private String playerUserName;

    @SerializedName("locationV1")
    private double v;

    @SerializedName("locationV2")
    private double v1;


    public PlayerLocation(String playerUserName, double v, double v1) {
        this.playerUserName = playerUserName;
        this.v = v;
        this.v1 = v1;
    }

    public double getV() {
        return v;
    }

    public double getV1() {
        return v1;
    }

    public String getPlayerUserName() {
        return playerUserName;
    }
}
