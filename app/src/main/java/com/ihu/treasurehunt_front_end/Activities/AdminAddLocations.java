package com.ihu.treasurehunt_front_end.Activities;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.os.Handler;

import com.google.android.material.button.MaterialButton;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.textfield.TextInputEditText;
import com.ihu.treasurehunt_front_end.Model.GameLocation;
import com.ihu.treasurehunt_front_end.R;
import com.ihu.treasurehunt_front_end.Requests.RetroFitCreate;
import com.ihu.treasurehunt_front_end.Service.AddLocationsGameService;

public class AdminAddLocations extends AppCompatActivity {

    TextInputEditText gameId ;
    TextInputEditText latitude ;
    TextInputEditText longitude;
    TextInputEditText title;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin_add_locations);

         gameId = findViewById(R.id.textGameId);
         latitude = findViewById(R.id.textLatitude);
         longitude = findViewById(R.id.textLongitude);
         title = findViewById(R.id.textTitle);

        MaterialButton complete = findViewById(R.id.btnComplete);
        MaterialButton back = findViewById(R.id.btnBack);

        complete.setOnClickListener(v ->{
            RetroFitCreate retroFitCreate =new RetroFitCreate();

                if (isTextViewsFilled())
                {

                    String id = gameId.getText().toString();
                    double v1 = Double.parseDouble(latitude.getText().toString());
                    double v2 = Double.parseDouble(longitude.getText().toString());
                    GameLocation location = new GameLocation(v1
                            ,v2
                            ,title.getText().toString());
                    AddLocationsGameService addLocationsGameService = new AddLocationsGameService();
                    addLocationsGameService.AddLocation(retroFitCreate.getJsonPlaceHolderAPI(),location,id);
                    new Handler().postDelayed(() ->{
                        Snackbar.make(v, addLocationsGameService.getMessage(),Snackbar.LENGTH_SHORT).show();
                    },500);
                }else
                    Snackbar.make(v,"Fill all text views",Snackbar.LENGTH_SHORT).show();


        });

        back.setOnClickListener(v->{
            clearEditTexts();
            finish();

        });
    }

    private Boolean isTextViewsFilled()
    {
        return !gameId.getText().toString().equals("")
                && !longitude.getText().toString().equals("")
                && !latitude.getText().toString().equals("")
                && !title.getText().toString().equals("");
    }

    private void clearEditTexts() {
        gameId.setText("");
        latitude.setText("");
        longitude.setText("");
        title.setText("");
    }

}