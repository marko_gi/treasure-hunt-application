package com.ihu.treasurehunt_front_end.Activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.snackbar.Snackbar;
import com.ihu.treasurehunt_front_end.Model.GameState;
import com.ihu.treasurehunt_front_end.Model.TreasureHuntGame;
import com.ihu.treasurehunt_front_end.R;
import com.ihu.treasurehunt_front_end.Requests.AddPointsRequest;
import com.ihu.treasurehunt_front_end.Requests.LeaderBoardList;
import com.ihu.treasurehunt_front_end.Requests.RetroFitCreate;
import com.ihu.treasurehunt_front_end.Service.TreasureHuntGameService;

public class GlobalGamesRiddleActivity extends AppCompatActivity {

    private RetroFitCreate retroFitCreate = new RetroFitCreate();
    LeaderBoardList leaderBoardList = new LeaderBoardList();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_global_games_riddle);

        EditText answerText = findViewById(R.id.AnswerText);
        TextView btnCheck = findViewById(R.id.btnCheckAnswer);
        TextView questionText = findViewById(R.id.QuestionText);

        Bundle extras = getIntent().getExtras();

        String question = (String) extras.get("Question");
        String answer = (String) extras.get("Answer");
        int points =  extras.getInt("Points");

        questionText.setText(question);

        btnCheck.setOnClickListener(v ->{
            if (answerText.getText().toString().equals(answer))
            {
                Toast.makeText(GlobalGamesRiddleActivity.this, "Correct Answer", Toast.LENGTH_SHORT).show();
                leaderBoardList.updateLeaderBoard(retroFitCreate.getJsonPlaceHolderAPI(),SignInActivity.loginUser.getName(),points);
                AdminsGamesMap.gameCalculations.addPoints(points);
            }
            Toast.makeText(this,"Wrong answer",Toast.LENGTH_SHORT).show();
            AdminsGamesMap.marker.setVisible(false);

            AdminsGamesMap.gameCalculations.incPositionOfLocation();
            if (AdminsGamesMap.gameCalculations.getTreasureHuntGame().getLocations().size()<=AdminsGamesMap.gameCalculations.getCurrentPositionOfLocation()) {
                finish();
                TreasureHuntGameService treasureHuntGameService = new TreasureHuntGameService();
                treasureHuntGameService.getGame(retroFitCreate.getJsonPlaceHolderAPI(), AdminsGamesMap.gameCalculations.getTreasureHuntGame().getId());
                new Handler().postDelayed(() -> {
                    if (treasureHuntGameService.getTreasureHuntGame().getWinner() == null) {
                        treasureHuntGameService.setWinner(retroFitCreate.getJsonPlaceHolderAPI()
                                , SignInActivity.loginUser
                                , AdminsGamesMap.gameCalculations.getTreasureHuntGame().getId());
                        Intent intent = new Intent(this, GameWinActivity.class);
                        intent.putExtra("WINNER", SignInActivity.loginUser.getName());
                        startActivity(intent);
                    } else {
                        Intent intent = new Intent(this, GameWinActivity.class);
                        intent.putExtra("WINNER", treasureHuntGameService.getTreasureHuntGame().getWinner().getName());
                        startActivity(intent);
                    }
                }, 500);
            }
            finish();
        });
    }
}