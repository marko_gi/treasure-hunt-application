package com.ihu.treasurehunt_front_end.Activities;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.FragmentActivity;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.ihu.treasurehunt_front_end.Activities.SettingsActivity.NameChange;
import com.ihu.treasurehunt_front_end.Dialogs.HintDialog;
import com.ihu.treasurehunt_front_end.R;
import com.ihu.treasurehunt_front_end.Requests.CheckUserState;
import com.ihu.treasurehunt_front_end.Requests.GetUserScoreRequest;
import com.ihu.treasurehunt_front_end.Requests.RequestLives;
import com.ihu.treasurehunt_front_end.Requests.ResetWatchTowerRequest;
import com.ihu.treasurehunt_front_end.Requests.RetroFitCreate;


public class MapsActivity extends FragmentActivity implements OnMapReadyCallback
{
    public TextView heart1;
    public TextView heart2;
    public TextView heart3;
    public TextView heart4;
    public TextView heart5;
    private MapMarkerOptions mapMarkerOptions = new MapMarkerOptions();
    private Button hintButton;
    private RetroFitCreate retroFitCreate = new RetroFitCreate();
    private CheckUserState checkUserState = new CheckUserState();
    private RequestLives requestLives = new RequestLives();
    private GetUserScoreRequest getUserScoreRequest = new GetUserScoreRequest();
    private ResetWatchTowerRequest resetWatchTowerRequest = new ResetWatchTowerRequest();
    private LocationListener locationListener;
    private LocationManager locationManager;
    private GameLoseActivity gameLoseActivity;
    protected static TextView textView;
    private GoogleMap mMap;
    Marker motionMarker = null;
    private LatLng latLng;
    protected static Marker marker;
    private  Marker casinoMarker;
    private  Marker watchTowerMarker;
    public static int cntlives=5;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        hintButton = (Button)findViewById(R.id.button);
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        requestLives.getLives(retroFitCreate.getJsonPlaceHolderAPI(), MainActivity.game.getUserLoggedIn());

        textView = (TextView) findViewById(R.id.textView2);
        heart1 = (TextView) findViewById(R.id.heart1);
        heart2 = (TextView) findViewById(R.id.heart2);
        heart3 = (TextView) findViewById(R.id.heart3);
        heart4 = (TextView) findViewById(R.id.heart4);
        heart5 = (TextView) findViewById(R.id.heart5);
        mapFragment.getMapAsync(this);
        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, PackageManager.PERMISSION_GRANTED);
        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_COARSE_LOCATION}, PackageManager.PERMISSION_GRANTED);
    }
    @SuppressLint("SetTextI18n")
    @Override
    protected void onResume() {
        super.onResume();
        requestLives.getLives(retroFitCreate.getJsonPlaceHolderAPI(), MainActivity.game.getUserLoggedIn());

        getUserScoreRequest.getUserScore(retroFitCreate.getJsonPlaceHolderAPI(),MainActivity.game.getUserLoggedIn());
        LivesUpdate(getCntlives());

        new Handler().postDelayed(() -> {
            MainActivity.game.setGameScore(getUserScoreRequest.getScore());
            textView.setText("Score : " + MainActivity.game.getGameScore());
            //cntlives=requestLives.getLives();
            MainActivity.game.setLives(requestLives.getLives());
           // LivesUpdate(getCntlives());
        }, 750);

    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.setMapType(2);
        final LatLng tei = new LatLng(41.076797, 23.553648);
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(tei, 5));

        marker = MainActivity.game.addFirstLocationToMap(mMap);
        casinoMarker = MainActivity.game.addCasinoLocationToMap(mMap);
        watchTowerMarker = MainActivity.game.addWatchTowerLocationToMap(mMap);
        requestLives.getLives(retroFitCreate.getJsonPlaceHolderAPI(), MainActivity.game.getUserLoggedIn());
        MainActivity.game.setLives(requestLives.getLives());
        LivesUpdate(getCntlives());

        locationListener = location -> {
          //  requestLives.getLives(retroFitCreate.getJsonPlaceHolderAPI(), MainActivity.game.getUserLoggedIn());

            MainActivity.game.setGameScore(getUserScoreRequest.getScore());
            textView.setText("Score : " + MainActivity.game.getGameScore());

            try {
                latLng = new LatLng(location.getLatitude(), location.getLongitude());
                if (motionMarker == null) {
                    MarkerOptions options = new MarkerOptions().position(latLng).title(MainActivity.game.getUserLoggedIn()).icon(mapMarkerOptions.bitmapDescriptorFromVector(getApplicationContext(),R.drawable.ic_baseline_person_pin_circle_24));
                    motionMarker = mMap.addMarker(options);
                } else {
                    motionMarker.setPosition(latLng);
                }
                mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(location.getLatitude(), location.getLongitude()),16 ));

                marker = MainActivity.game.addFirstLocationToMap(mMap);
                MainActivity.game.DistanceBetween(latLng,casinoMarker);
                MainActivity.game.DistanceBetween(latLng,marker);

            } catch (SecurityException e) {
                e.printStackTrace();
            }
        };

        locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
        try {
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 1000, 5, locationListener);
        } catch (SecurityException e) {
            e.printStackTrace();
        }

        mMap.setOnMarkerClickListener(marker -> {
            checkUserState.checkUserState(retroFitCreate.getJsonPlaceHolderAPI());
            if (marker.getTitle().equals(MainActivity.game.getUserLoggedIn())) {
                Toast.makeText(MapsActivity.this, "It's You", Toast.LENGTH_SHORT).show();
                return false;
            }
            else if (marker.getTitle().equals("end")){
                resetWatchTowerRequest.resetWatchTower(retroFitCreate.getJsonPlaceHolderAPI());
                Toast.makeText(MapsActivity.this, "YOU WON!!", Toast.LENGTH_SHORT).show();
                Intent intent = new Intent (MapsActivity.this, GameWinActivity.class);
                intent.putExtra("WINNER",MainActivity.game.getUserLoggedIn());
                startActivity(intent);
                return false;
            }
            else if (marker.getTitle().equals("Casino")){

                Intent intent = new Intent (MapsActivity.this, CasinoActivity.class);
                startActivity(intent);
                return false;}
            else if (marker.getTitle().equals("WatchTower")){

                Intent intent = new Intent (MapsActivity.this, WatchTowerActivity.class);
                startActivity(intent);
                return false;}
            else {
                new Handler().postDelayed(() -> {
                    String stateUserToWIN = checkUserState.getUserToWIN();
                    if (stateUserToWIN.equals("PLAYING")) {
                        startActivity(new Intent(MapsActivity.this, RiddleActivity.class));
                    } else {
                        Intent intent = new Intent (MapsActivity.this, GameWinActivity.class);
                        intent.putExtra("WINNER", stateUserToWIN);
                        startActivity(intent);
                    }
               }, 750);
                return false;
            }
        });

        hintButton.setOnClickListener(v -> openDialog());

    }
    void openDialog(){
        HintDialog hintDialog = new HintDialog();
        hintDialog.show(getSupportFragmentManager(),"Hint dialog");
    }
    public int getCntlives() {
        return cntlives;
    }

    public void setCntlives(int cntlives) {
        this.cntlives = cntlives;
    }
    public void LivesUpdate ( int lives){
        if (lives == 5) {
            heart1.setVisibility(View.VISIBLE);
            heart2.setVisibility(View.VISIBLE);
            heart3.setVisibility(View.VISIBLE);
            heart4.setVisibility(View.VISIBLE);
            heart5.setVisibility(View.VISIBLE);
        }
        if (lives == 4) {
            heart1.setVisibility(View.INVISIBLE);
            heart2.setVisibility(View.VISIBLE);
            heart3.setVisibility(View.VISIBLE);
            heart4.setVisibility(View.VISIBLE);
            heart5.setVisibility(View.VISIBLE);
        }
        if (lives == 3) {
            heart1.setVisibility(View.INVISIBLE);
            heart2.setVisibility(View.INVISIBLE);
            heart3.setVisibility(View.VISIBLE);
            heart4.setVisibility(View.VISIBLE);
            heart5.setVisibility(View.VISIBLE);
        }
        if (lives == 2) {
            heart1.setVisibility(View.INVISIBLE);
            heart2.setVisibility(View.INVISIBLE);
            heart3.setVisibility(View.INVISIBLE);
            heart4.setVisibility(View.VISIBLE);
            heart5.setVisibility(View.VISIBLE);
        }
        if (lives == 1) {
            heart1.setVisibility(View.INVISIBLE);
            heart2.setVisibility(View.INVISIBLE);
            heart3.setVisibility(View.INVISIBLE);
            heart4.setVisibility(View.INVISIBLE);
            heart5.setVisibility(View.VISIBLE);
        }
        if (lives == 0) {
            heart1.setVisibility(View.INVISIBLE);
            heart2.setVisibility(View.INVISIBLE);
            heart3.setVisibility(View.INVISIBLE);
            heart4.setVisibility(View.INVISIBLE);
            heart5.setVisibility(View.INVISIBLE);
            Intent intent = new Intent (MapsActivity.this, GameLoseActivity.class);
            startActivity(intent);
        }
    }
}
