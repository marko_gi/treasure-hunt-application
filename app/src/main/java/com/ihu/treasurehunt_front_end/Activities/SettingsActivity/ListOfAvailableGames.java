package com.ihu.treasurehunt_front_end.Activities.SettingsActivity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.google.android.material.snackbar.Snackbar;
import com.ihu.treasurehunt_front_end.Activities.AdminsGamesMap;
import com.ihu.treasurehunt_front_end.Activities.MainActivity;
import com.ihu.treasurehunt_front_end.Activities.SignInActivity;
import com.ihu.treasurehunt_front_end.Model.AvailableGames;
import com.ihu.treasurehunt_front_end.Model.GameCalculations;
import com.ihu.treasurehunt_front_end.Model.GameState;
import com.ihu.treasurehunt_front_end.Model.TreasureHuntGame;
import com.ihu.treasurehunt_front_end.R;
import com.ihu.treasurehunt_front_end.Requests.GamesList;
import com.ihu.treasurehunt_front_end.Requests.RetroFitCreate;
import com.ihu.treasurehunt_front_end.Service.TreasureHuntGameService;

import java.util.ArrayList;
import java.util.List;

public class ListOfAvailableGames extends AppCompatActivity {



    private TextView textViewGames;
    private TextView textViewCode;
    private final RetroFitCreate retroFitCreate = new RetroFitCreate();
    private TextView idGames;
    private  TreasureHuntGameService treasureHuntGameService;
    public static GameCalculations gameCalculations;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        GamesList gamesList = new GamesList();
        setContentView(R.layout.activity_list_of_available_games);
        textViewGames = findViewById(R.id.Games);
        textViewCode = findViewById(R.id.Code);
        idGames = findViewById(R.id.idGames);
        Button btnPlayGame = findViewById(R.id.playGame);

        gamesList.getAvailableGames(retroFitCreate.getJsonPlaceHolderAPI());
        new Handler().postDelayed(()->{
            for(int i =0; i<gamesList.getAvailableGames().size();i++){
                textViewGames.append(gamesList.getAvailableGames().get(i).getGameLocation()+"\n");
                textViewCode.append(gamesList.getAvailableGames().get(i).getid()+"\n");
            }
        },1000);

        btnPlayGame.setOnClickListener(v -> {
            gamesList.addPlayerToTheGame(retroFitCreate.getJsonPlaceHolderAPI()
                    , SignInActivity.loginUser.getName()
                    , idGames.getText().toString());
            new Handler().postDelayed(()->{
                if (gamesList.getAddPlayer())
                {
                    treasureHuntGameService= new TreasureHuntGameService();
                   treasureHuntGameService.getGame(retroFitCreate.getJsonPlaceHolderAPI(),idGames.getText().toString());
                   new Handler().postDelayed(()->{
                       if (!treasureHuntGameService.getTreasureHuntGame().getGameState().equals(GameState.Over))
                       {
                           gameCalculations = new GameCalculations(treasureHuntGameService);
                           startActivity(new Intent(this, AdminsGamesMap.class));
                       }
                       Snackbar.make(v,"Game is over",Snackbar.LENGTH_SHORT).show();
                    },500);
                }else
                    Snackbar.make(v,"Game does'nt exists",Snackbar.LENGTH_SHORT).show();

            },1000);


        });
    }
}