package com.ihu.treasurehunt_front_end.Activities;

import androidx.core.app.ActivityCompat;
import androidx.fragment.app.FragmentActivity;
import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;

import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.ihu.treasurehunt_front_end.Activities.SettingsActivity.ListOfAvailableGames;
import com.ihu.treasurehunt_front_end.Dialogs.HintAdminsGames;
import com.ihu.treasurehunt_front_end.Dialogs.HintDialog;
import com.ihu.treasurehunt_front_end.Model.GameCalculations;
import com.ihu.treasurehunt_front_end.Model.GameState;
import com.ihu.treasurehunt_front_end.Model.PlayerLocation;
import com.ihu.treasurehunt_front_end.Model.Question;
import com.ihu.treasurehunt_front_end.Model.Status;
import com.ihu.treasurehunt_front_end.Model.TreasureHuntGame;
import com.ihu.treasurehunt_front_end.Model.User;
import com.ihu.treasurehunt_front_end.R;
import com.ihu.treasurehunt_front_end.Requests.RequestRandomQuestion;
import com.ihu.treasurehunt_front_end.Requests.RetroFitCreate;
import com.ihu.treasurehunt_front_end.Service.TreasureHuntGameService;

import java.util.List;


public class AdminsGamesMap extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap mMap;
    public static Marker marker;
    private LocationListener locationListener;
    private LocationManager locationManager;
    private LatLng latLng;
    private Marker myLocation = null;
    public static GameCalculations gameCalculations = ListOfAvailableGames.gameCalculations;
    private RetroFitCreate retroFitCreate = new RetroFitCreate();
    private MapMarkerOptions mapMarkerOptions = new MapMarkerOptions();
    private User loginUser = SignInActivity.loginUser;
    private TextView score ;
    private Button btnHint;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admins_games_map);
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, PackageManager.PERMISSION_GRANTED);
        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_COARSE_LOCATION}, PackageManager.PERMISSION_GRANTED);
        score = findViewById(R.id.scoreText);
        btnHint = findViewById(R.id.hint);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.setMapType(2);
        RequestRandomQuestion requestRandomQuestion = new RequestRandomQuestion();
        requestRandomQuestion.getRandomQuestion(retroFitCreate.getJsonPlaceHolderAPI());
        new Handler().postDelayed(() ->{
            gameCalculations.setCurrentQuestion(requestRandomQuestion.getQuestion());
        },1000);

        locationListener = location -> {
            RequestRandomQuestion requestRandomQuestion1 = new RequestRandomQuestion();
            requestRandomQuestion1.getRandomQuestion(retroFitCreate.getJsonPlaceHolderAPI());
            new Handler().postDelayed(() ->{
                gameCalculations.setCurrentQuestion(requestRandomQuestion1.getQuestion());
            },1000);
            score.setText("Score : " + gameCalculations.getGameScore());
            TreasureHuntGameService treasureHuntGameService = new TreasureHuntGameService();
            treasureHuntGameService.getGame(retroFitCreate.getJsonPlaceHolderAPI(),gameCalculations.getTreasureHuntGame().getId());
            new Handler().postDelayed(()->
                    gameCalculations.setTreasureHuntGame(treasureHuntGameService.getTreasureHuntGame()),1500);
            try {
                latLng = new LatLng(location.getLatitude(), location.getLongitude());
                PlayerLocation playerLocation = new PlayerLocation(SignInActivity.loginUser.getName(),location.getLatitude(),location.getLongitude());
                gameCalculations.getTreasureHuntGameService().addPlayerLocation(retroFitCreate.getJsonPlaceHolderAPI(),
                        playerLocation
                        ,gameCalculations.getTreasureHuntGame().getId());

                if (myLocation == null) {
                    MarkerOptions options =
                            new MarkerOptions().position(latLng).title(loginUser.getName())
                                    .icon(mapMarkerOptions.bitmapDescriptorFromVector(getApplicationContext(),R.drawable.ic_baseline_person_pin_circle_24));
                    myLocation = mMap.addMarker(options);

                } else {
                   myLocation.setPosition(latLng);
                }

                List<PlayerLocation> locations = gameCalculations.OthersPlayersLocation();
                if (locations.size() > 0)
                {
                    gameCalculations.othersPlayersMarker(googleMap);
                }
                mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(location.getLatitude(), location.getLongitude()),15 ));
                marker = gameCalculations.addLocationToMap(mMap);
                gameCalculations.DistanceBetween(latLng,marker);

            } catch (SecurityException e) {
                e.printStackTrace();
            }
        };

        locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
        try {
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 1000, 5, locationListener);
        } catch (SecurityException e) {
            e.printStackTrace();
        }

        mMap.setOnMarkerClickListener(marker -> {
            if (marker.getTitle().equals(loginUser.getName())) {
                Toast.makeText(AdminsGamesMap.this, "You are here", Toast.LENGTH_SHORT).show();
                return false;
            }else if(marker.getTitle().equals(gameCalculations.getTreasureHuntGame().getLocations().get(gameCalculations.getCurrentPositionOfLocation()).getTitle()))
            {
                if (gameCalculations.getTreasureHuntGame().getWinner() == null)
                {
                    Intent intent = new Intent(this,GlobalGamesRiddleActivity.class);
                    Bundle bundle = new Bundle();
                    bundle.putString("Question",gameCalculations.getCurrentQuestion().getQuestion());
                    bundle.putString("Answer",gameCalculations.getCurrentQuestion().getAnswer());
                    bundle.putInt("Points",gameCalculations.getCurrentQuestion().getPoints());
                    intent.putExtras(bundle);
                    startActivity(intent);
                }
            } else
            {
                for (int i=0; i<gameCalculations.getTreasureHuntGame().getPlayers().size();i++)
                {
                    if (marker.getTitle().equals(gameCalculations.getTreasureHuntGame().getPlayers().get(i).getName())) {
                        Toast.makeText(this, "Hunter : " + gameCalculations.getTreasureHuntGame().getPlayers().get(i).getName(), Toast.LENGTH_SHORT).show();
                    }
                }
            }

            return false;
        });

        btnHint.setOnClickListener(v -> openDialog());
    }

    void openDialog(){
        HintAdminsGames hintAdminsGames=
                new HintAdminsGames(gameCalculations.getTreasureHuntGame().
                        getLocations().get(gameCalculations.getCurrentPositionOfLocation())
                        .getTitle());
        hintAdminsGames.show(getSupportFragmentManager(),"Hint dialog");
    }

    @SuppressLint("SetTextI18n")
    @Override
    protected void onResume() {
        super.onResume();
        score.setText("Score : " + gameCalculations.getGameScore());
    }

}